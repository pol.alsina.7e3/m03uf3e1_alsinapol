﻿using System;
using System.Collections.Generic;
using System.IO;

namespace M03UF3E1_AlsinaPol
{

    class GestioDeDirectoris
    {
        /// <summary>
        /// Metode que inicia tot el programa
        /// </summary>
        public static void Main()
        {
            GestioDeDirectoris gestioDeDirectoris = new GestioDeDirectoris();
            gestioDeDirectoris.WhileMenu();
        }
        /// <summary>
        /// Metode que incialitzar el menu
        /// </summary>
        public void WhileMenu()
        {
            bool finish;
            do
            {
                MostrarMenu();
                finish = TractarOpcio();
            } while (!finish);
        }
        /// <summary>
        /// Metode que tractar les diferents opcions que te el menu i accedeix a  altres metodes
        /// </summary>
        /// <returns>Retorna un boolea. Si es true acabar el programa, si no continua</returns>
        public bool TractarOpcio()
        {
            int opcio = OpcioInt(4);
            switch (opcio)
            {
                case 1:
                    ExisteixCarpetaEsFitxerOCarpeta();
                    break;
                case 2:
                    LlistarDirectoris();
                    break;
                case 3:
                    DeleteRemoveCreateDirectory();
                    break;
                case 4:
                    ListFilesWithDetails();
                    break;
                case 0:
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Metode que imprimeix per consola les diferents opcions del menu
        /// </summary>
        public void MostrarMenu()
        {
            Console.WriteLine("-----Gestior de Directoris-----");
            Console.WriteLine("1-Comprobar si existeix directori/fitxer");
            Console.WriteLine("2-Llistat de Fitxers");
            Console.WriteLine("3-Crear,borrar o canviar nom a un directori");
            Console.WriteLine("4-Llistat de fitxers pero, amb mes informacio");
            Console.WriteLine("0-Per sortir del programa");
        }
        /// <summary>
        /// Metode que llegeix un numero per a al menu
        /// </summary>
        /// <returns>Retorna un numero que equival a l'opcio del menu</returns>
        public int OpcioInt(int maxNum)
        {
            int opcio;
            do
            {
                opcio = Convert.ToInt32(Console.ReadLine());
            } while (opcio < 0 || opcio > maxNum);
            return opcio;
        }
        /// <summary>
        /// Metode que llegeix strings
        /// </summary>
        /// <param name="missatge">Param string conte un frase per guiar l'usuari</param>
        /// <returns>Retorna un string donat per l'usuari</returns>
        public string LlegirString(string missatge)
        {
            Console.WriteLine(missatge);
            return Console.ReadLine();
        }
        /// <summary>
        /// Metode que imprimeix per pantalla si x existeix o no
        /// </summary>
        /// <param name="missatge">Param que conte el qie existeix</param>
        public void PassarMissatgeConsolaExisteix(string missatge)
        {
            Console.WriteLine("Aquest {0} existeix", missatge);
        }
        /// <summary>
        /// Metode que mostra el path absolut d'una carpeta
        /// </summary>
        /// <param name="target">Param que conte el path al qual vols aconseguir el seu npath absolut</param>
        public void PathAbsolut(string target)
        {
            Console.WriteLine(Path.GetFullPath(target));
        }
        /// <summary>
        /// Metode que mostra l'actual posicio en les carpetas
        /// </summary>
        public void MostrarPosicioActual()
        {
            Console.WriteLine("Tingues en conte que ara mateix estas a: " + Environment.CurrentDirectory);
        }
        //Ex1
        /// <summary>
        /// Metode que indica si un fitxer o una carpeta existeix i diu de quin tipus es
        /// </summary>
        public void ExisteixCarpetaEsFitxerOCarpeta()
        {
            MostrarPosicioActual();
            string target = LlegirString("Dona'm el nom d'una carpeta o ftixer i et dire si existeis o no:");
            ComprobarExistencia(target);
            PathAbsolut(target);
        }
        /// <summary>
        /// Metode que comprova si un fitxer o un directori exiteix
        /// </summary>
        /// <param name="target">Param path del fitxer o carpeta</param>
        public void ComprobarExistencia(string target)
        {
            if (Directory.Exists(target)) PassarMissatgeConsolaExisteix("directori");
            else if (File.Exists(target)) PassarMissatgeConsolaExisteix("fitxer");
            else PassarMissatgeConsolaExisteix("directori o fitxer NO");
        }

        //Ex2
        /// <summary>
        /// Inici del ex2 demana al path de la carpeta a llistar
        /// </summary>
        public void LlistarDirectoris()
        {
            MostrarPosicioActual();
            string target = LlegirString("Dona'm el path d'una carpeta:");
            LlistarCarpetasIFitxers(target);
        }
        /// <summary>
        /// Mostra al llistat dels diferents directoris i fitxers que hi ha dins d'una carpeta
        /// </summary>
        /// <param name="target">path de la carpeta on mostrar al llistat</param>
        void LlistarCarpetasIFitxers(string target)
        {
            List<string> dirs = new List<string>(Directory.EnumerateDirectories(target));
            IEnumerable<string> files = Directory.EnumerateFiles(target, "*.", SearchOption.TopDirectoryOnly);

            Console.WriteLine("Folders in {0}", target);
            foreach (var dir in dirs) Console.WriteLine(dir);

            Console.WriteLine("Files in {0}", target);
            foreach (var file in files) Console.WriteLine(file);
        }

        //Ex3
        /// <summary>
        /// Inici del ex3
        /// </summary>
        void DeleteRemoveCreateDirectory()
        {
            MostrarPosicioActual();
            string target = LlegirString("Dona'm el path d'una carpeta:");
            bool finish;
            do
            {
                OpcionsEx3();
                finish = Ex3Opcio(target);
            } while (!finish);
        }
        /// <summary>
        /// Menu del ex3 amb les diferents execucions
        /// </summary>
        /// <param name="target">path de la carpeta on es treballara</param>
        /// <returns>retorna un bool per saber si sortir del mini programa</returns>
        public bool Ex3Opcio(string target)
        {
            int opcio = OpcioInt(3);
            switch (opcio)
            {
                case 1:// crear directori
                    Directory.CreateDirectory(target);
                    break;
                case 2://Canviar nom directori
                    Directory.SetCurrentDirectory(target);
                    string nouNom = LlegirString("Ara dona'm el nou nom la carpeta:");
                    Directory.Move(target, nouNom);
                    break;
                case 3:// Esborrar directori
                    Directory.Delete(target);
                    break;
                case 0:
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Mostrar les diferents opcions del menu del ex3
        /// </summary>
        void OpcionsEx3()
        {
            Console.WriteLine("-----Ex3-----");
            Console.WriteLine("1-Crear Directori");
            Console.WriteLine("2-Canvir nom del directori");
            Console.WriteLine("3-Esborrar el directori");
            Console.WriteLine("0-Per sortir del programa");
        }

       //Ex4
       /// <summary>
       /// Llegeix tot el contigut d'una carpeta amb detalls
       /// </summary>
       void ListFilesWithDetails()
        {
            MostrarPosicioActual();
            string target = LlegirString("Dona'm el path d'una carpeta:");
            DirectoryInfo carpeta = new DirectoryInfo(target);
            DirectoryInfo[] dires = carpeta.GetDirectories();
            FileInfo[] files = carpeta.GetFiles();

            foreach (var dir in dires)
            {
                Console.WriteLine("Name: {0}", dir.Name);
                Console.WriteLine(dir.Attributes);
                Console.WriteLine("Last Time Modified: {0}", dir.LastWriteTimeUtc);

            }
                foreach (var file in files)
            {
                Console.WriteLine(file.Name);
                Console.WriteLine("Size: {0}", file.Length);
                Console.WriteLine("Last Time Modified: {0}", file.LastWriteTimeUtc);

            }

        }
    }
}
